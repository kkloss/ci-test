require 'bundler/setup'
require 'dotenv/load'
require 'gitlab'

puts Gitlab.run_trigger(
  '58462019',
  ENV.fetch('CI_JOB_TOKEN'),
  'main',
  {}
).inspect
